import React from 'react';
import { StyledTopBar } from './styled';

const TopBar = (): React.ReactElement => <StyledTopBar>top bar</StyledTopBar>;

export default TopBar;
