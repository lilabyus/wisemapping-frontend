import styled from 'styled-components';
import { times } from '../../size';

export const StyledTopBar = styled.div`
    height: ${times(10)};
    width: 100%;
    border: 1px solid black;
`;
