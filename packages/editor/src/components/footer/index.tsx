import React from 'react';
import { StyledFooter } from './styled';

const Footer = (): React.ReactElement => <StyledFooter>footer</StyledFooter>;

export default Footer;
