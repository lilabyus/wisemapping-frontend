import React from 'react';
import { StyledCanvas } from './styled';

const Canvas = (): React.ReactElement => <StyledCanvas>canvas</StyledCanvas>;

export default Canvas;
