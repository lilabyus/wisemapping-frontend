import styled from 'styled-components';

export const StyledCanvas = styled.div`
  height: 100%
  width: 100%;
  flex: 1;
 
`;
