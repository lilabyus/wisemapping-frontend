import MenuItem from '@material-ui/core/MenuItem';
import withStyles from '@material-ui/core/styles/withStyles';

export const StyledMenuItem = withStyles({
    root: {
        width: '300px',
    },
})(MenuItem);
