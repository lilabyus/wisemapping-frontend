const fadeEffect = require('./FadeEffect').default;
const shape = require('./Shape').default;

export const Utils = {
    FadeEffect: fadeEffect,
    Shape: shape,
};
